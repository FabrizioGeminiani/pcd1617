package pcd.lab03.factorizer.stateless;

import pcd.lab03.factorizer.FactorizerService;
import pcd.lab03.factorizer.ServiceUser;

public class BasicTest {

	public static void main(String[] args) {

		test(102);
		
	}
	

	private static void test(long value){
		FactorizerService service = new StatelessFactorizer();
		int[] factors = service.getFactors(value);
		System.out.println("Value: "+value);
		System.out.println("Factors: ");
		for (int x: factors){
			System.out.print(x+" ");
		}
		System.out.println();
	}


}
